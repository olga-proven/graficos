import { Component, Output, EventEmitter  } from '@angular/core';
import { Covid, CovidModel  } from 'src/app/models/covid.module';
import { SendDataService } from '../services/send-data.service';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  template: ` <app-table [dataForTable]=data></app-table> `,
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent {
  @Output() dataToParent = new EventEmitter<Covid[]>();
  mostrarBarraProgreso = false;
  progresoCarga = 0;
  file: any;
  fileContent: any;
  data: Covid[] = [];
  
 constructor(private sendDataService:SendDataService){

  
 }
  uploadFile(event: any){
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
 
      reader.onload = (e) => {
        this.fileContent = e.target?.result;
        console.log('File Content:', this.fileContent);
        this.parseFile();
      };
 
      reader.readAsText(file); // Assuming you want to read the file as text, adjust as needed
    }
  }
 
  
  parseFile(){
    const rows = this.fileContent.split('\n');
    const headers = rows[0].split(',');
    console.log(headers);
 
    for (let i = 1; i < rows.length; i++) {
      const values = rows[i].split(',');
      const covidData: Covid = new CovidModel();
 
      for (let j = 0; j < headers.length && rows.length > 1; j++) {
        const key = headers[j].trim() as keyof Covid;
        covidData[key] = values[j].trim() as never;
        // covidData[key] = this.convertToNumber(values[j].trim()) as never;
      }
      this.data.push(covidData);
    }
    this.dataToParent.emit(this.data);
    // this.sendDataService.setArray(this.data);
 
    console.log('Parsed Data:', this.data);
  }
 
  private convertToNumber(value: string): number {
    const numericValue = parseFloat(value);
    return isNaN(numericValue) ? 0 : numericValue;
  }
 
}
