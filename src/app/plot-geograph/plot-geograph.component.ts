import { Component, Input, OnInit, SimpleChanges} from '@angular/core';
import { Covid } from '../models/covid.module';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-plot-geograph',
  templateUrl: './plot-geograph.component.html',
  styleUrls: ['./plot-geograph.component.css']
})
export class PlotGeographComponent {
  @Input() dataForGeo: Covid[] = [];
  public geo: any;
  data10countries: Covid[] = [];


  
  ngOnChanges(changes: SimpleChanges) {
    if (this.dataForGeo.length > 1) this.createGeo()
  }

  createGeo() {

    const data10countries = this.dataForGeo.slice(2, 12);
    console.log(data10countries);
    const labels = data10countries.map(countryData => countryData.country);
    const totalCases = data10countries.map(countryData => countryData.totalCases);
    const totalTests = data10countries.map(countryData => countryData.totalTests);
    const activeCases = data10countries.map(countryData => countryData.activeCases);
 
    if(this.geo) this.geo.destroy();
   

this.geo = new Chart("GeoChart", {
        type: 'bar', //this denotes tha type of chart
  
        data: {// values on X-Axis
          labels: labels, 
           datasets: [
            {
              label: "total cases",
              data: totalCases,
              backgroundColor: 'rgb(75, 192, 192)',
              borderWidth: 1,
        
              
              borderRadius: Number.MAX_VALUE,
              borderSkipped: false,
            },
            
            {
              label: "activeCases",
              data: activeCases,
              backgroundColor: 'rgb(75, 134, 192)',
             
              borderWidth: 3,
              
              borderRadius: Number.MAX_VALUE,
              borderSkipped: false,
            },

            {
              label: "totalTests",
              data: totalTests,
              backgroundColor: 'rgb(192, 75, 192)',
             
              borderWidth: 3,
              
              borderRadius: Number.MAX_VALUE,
              borderSkipped: false,
            },
          ],
          
        },
        options: {
          aspectRatio:2.5
        }
        
      }); 
    }
}
