import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlotCircularComponent } from './plot-circular.component';

describe('PlotCircularComponent', () => {
  let component: PlotCircularComponent;
  let fixture: ComponentFixture<PlotCircularComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlotCircularComponent]
    });
    fixture = TestBed.createComponent(PlotCircularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
