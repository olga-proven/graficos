import { Component, Input, SimpleChanges } from '@angular/core';
import { Covid } from '../models/covid.module';
import Chart from 'chart.js/auto';


@Component({
  selector: 'app-plot-circular',
  templateUrl: './plot-circular.component.html',
  styleUrls: ['./plot-circular.component.css']
})
export class PlotCircularComponent {
  @Input() dataForCircular: Covid[] = [];
  public geo: any;
  data10countries: Covid[] = [];


  
  ngOnChanges(changes: SimpleChanges) {
    if (this.dataForCircular.length > 1) this.createPie()
  }

  createPie() {

    const data10countries = this.dataForCircular.slice(12, 21);
    console.log(data10countries);
    const labels = data10countries.map(countryData => countryData.country);
    const totalCases = data10countries.map(countryData => countryData.totalCases);
    //const totalRecovered = data10countries.map(countryData => countryData.totalRecovered);
    
    if(this.geo) this.geo.destroy();

this.geo = new Chart("PieChart", {
        type: 'pie', //this denotes tha type of chart
  
        data: {// values on X-Axis
          labels: labels, 
           datasets: [
            {
              label: "total cases",
              data: totalCases,
              backgroundColor: this.generateRandomColors(totalCases.length),
              borderWidth: 1,
            },
            

          ],
          
        },
        options: {
          aspectRatio:2.5
        }
        
      }); 

      
    }
    generateRandomColors(numColors: number): string[] {
      const colors = [];
      for (let i = 0; i < numColors; i++) {
        const randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
        colors.push(randomColor);
      }
      return colors;
    }
}
