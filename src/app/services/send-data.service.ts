import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Covid, CovidModel  } from 'src/app/models/covid.module';

@Injectable({
  providedIn: 'root'
})
export class SendDataService {

  private dataService: BehaviorSubject<Covid[]> = new BehaviorSubject<Covid[]>([]);




  constructor() { }
  setArray(datos: Covid[] = []) {
    this.dataService.next(datos);
    console.log("en ser array" + this.dataService)
    console.log("en ser array", this.dataService.asObservable());
  }

  getArray() {
    return this.dataService.asObservable();
  }

  }

  