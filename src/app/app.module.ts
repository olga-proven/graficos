import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { TableComponent } from './table/table.component';
import { PlotGeographComponent } from './plot-geograph/plot-geograph.component';
import { PlotColumnsComponent } from './plot-columns/plot-columns.component';
import { PlotCircularComponent } from './plot-circular/plot-circular.component';

@NgModule({
  declarations: [
    AppComponent,
    UploadFileComponent,
    TableComponent,
    PlotGeographComponent,
    PlotColumnsComponent,
    PlotCircularComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
