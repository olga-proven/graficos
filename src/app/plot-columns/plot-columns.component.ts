import { Component, Input, SimpleChanges } from '@angular/core';
import { Covid } from '../models/covid.module';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-plot-columns',
  templateUrl: './plot-columns.component.html',
  styleUrls: ['./plot-columns.component.css']
})
export class PlotColumnsComponent {
  @Input() dataForColumns: Covid[] = [];
  public chart: any;
  data10countries: Covid[] = [];





  ngOnChanges(changes: SimpleChanges) {
    if (this.dataForColumns.length > 1) this.createColumns()
  }

  createColumns() {

    const data10countries = this.dataForColumns.slice(2, 12);
    console.log(data10countries);
    const labels = data10countries.map(countryData => countryData.country);
    const totalCases = data10countries.map(countryData => countryData.totalCases);
    const totalRecovered = data10countries.map(countryData => countryData.totalRecovered);
    
    if(this.chart) this.chart.destroy();

this.chart = new Chart("BarChart", {
        type: 'bar', //this denotes tha type of chart
  
        data: {// values on X-Axis
          labels: labels, 
           datasets: [
            {
              label: "total cases",
              data: totalCases,
              backgroundColor: 'rgb(75, 192, 192)',
              borderWidth: 1,
            },
            
            {
              label: "total recovered",
              data: totalRecovered,
              backgroundColor: 'rgb(153, 102, 255)',
             
              borderWidth: 1,
            },
          ],
          
        },
        options: {
          aspectRatio:2.5
        }
        
      }); 
    }



  
}
