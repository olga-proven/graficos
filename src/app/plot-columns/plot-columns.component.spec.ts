import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlotColumnsComponent } from './plot-columns.component';

describe('PlotColumnsComponent', () => {
  let component: PlotColumnsComponent;
  let fixture: ComponentFixture<PlotColumnsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlotColumnsComponent]
    });
    fixture = TestBed.createComponent(PlotColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
