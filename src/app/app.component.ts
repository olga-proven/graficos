import { Component } from '@angular/core';
import { Covid } from './models/covid.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'graficos';
  globalData: Covid[] = [];

  setDataToParent(data: Covid[]){
    this.globalData = data;
  }
}
